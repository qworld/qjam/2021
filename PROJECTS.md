![](images/qjam2021header.jpg)

# Completed Projects

[[_TOC_]]

## Quantum Music | QM

_**Awarded by Certificate of Excellence**_

**Team Members:**
- Devanshu Garg
- Sai Nandan Morapakula
- Sairupa Thota
- Samuel Zahorec
- Shreya Satsangi

**Mentor:** Ozlem Salehi

**Details:** https://gitlab.com/qworld/qjam/2021/-/issues/7

**Repository:** https://github.com/wait-a-sec/Quantum-Music-using-Random-Walks

**Presentation:** https://gitlab.com/qworld/qjam/2021/uploads/768f46bf9b18077d89894eca0bab94c6/Quantum_Computer_Music_-2.pdf

**Video:** https://youtu.be/bOz2IEGwupY

**Quantum Music Produced:** https://youtu.be/SJzoIo9nogo

## CatBits | QGreece Department

_**Awarded by Certificate of Excellence**_

**Team Members:**
- Ioannis Theodonis
- Georgia Sourpi

**Mentor:** Vishal Bajpe

**Details:** https://gitlab.com/qworld/qjam/2021/-/issues/12

**Repository:** https://github.com/zina2001/Catbits

**Presentation:** https://drive.google.com/file/d/1c-ZPNz8ADMZlW5A1aOM-0VcopWirYbni/view?usp=sharing

**Video Link:** https://drive.google.com/file/d/1c-ZPNz8ADMZlW5A1aOM-0VcopWirYbni/view?usp=sharing

**Storyboard:** https://github.com/igelover/qolab/blob/main/docs/Storyboard.pdf

## Bloch duel | Cavecoders

_**Awarded by "Certificate of Distinction"**_

**Team Members:**
- Przemysław Michałowski
- Krzysztof Królewicz
- Vismaya Sunil

**Mentor:** Claudia Zendejas-Morales

**Details:** https://gitlab.com/qworld/qjam/2021/-/issues/4

**Repository:** https://gitlab.com/michalowski.prz/bloch-duel/-/tree/main

**Presentation:** https://gitlab.com/michalowski.prz/bloch-duel/-/blob/main/Bloch_Duel_-_Team_Cavecoders.pdf

**Video:**  https://drive.google.com/drive/folders/15JX9guDSb1EXjcLdOrkwlth0B7Ed-peu?fbclid=IwAR1LA0ZoXgugUth6V5HgH9TL0-_HTVMrCOzDSP2ZaV1LlXnSDW1WypXd4ss

## Effect of Correlations on Noisy states | Quantum Blinders

_**Awarded by "Certificate of Distinction"**_

**Team Members:**
- Abhishek Bhardwaj
- Ranendu Adhikary
- Dimitra Nikolaidou
- Hariprasad Madathil
- Nicole Farrier
- Soumyabrata Hazra

**Mentor:** Lorraine Majiri

**Details:** https://gitlab.com/qworld/qjam/2021/-/issues/5

**Repository:** https://github.com/Bharduabhi/Quantum_Blinders

**Presentation:** https://drive.google.com/file/d/1Vvd1f6ObGTL-AiXx52lEYvs9Nhz1h4aM/view?usp=sharing

**Video:** https://drive.google.com/file/d/1Dm9JyXRgg5dPDLysfsxkCnz8JsfbKGRn/view?usp=sharing

## A quantum open-science collaboration environment | Qolab

_**Awarded by "Certificate of Distinction"**_

**Team Members:**
- Israel Gelover
- Subhra Priyadarshini Behura
- Muhammad Usaid
- Manvi Gusain

**Mentor:** Adam Glos

**Details:** https://gitlab.com/qworld/qjam/2021/-/issues/10

**Repository:** https://github.com/igelover/qolab

**Presentation:** https://github.com/igelover/qolab/blob/main/docs/QolabPresentation.pdf

**Video:** https://youtu.be/g-cP3JjrIWo

**Storyboard:** https://github.com/igelover/qolab/blob/main/docs/Storyboard.pdf

## A Study in Computer Vision and Quantum Computing | SCVQC

_**Awarded by Certificate of Merit**_

**Team Members:**
- Kamil Dyga
- Bill Gonzalez
- Luis Gerardo Ayala Bertel
- Başak Ekinci

**Mentors:** Kenneth Isamade and Kareem El Safty

**Details:** https://gitlab.com/qworld/qjam/2021/-/issues/9

**Repository:** https://github.com/WebheadTech/QJam2021

**Presentation:** https://drive.google.com/file/d/1HFMXMSbuZDpebC6jFIkYgbKUS19r1eiW/view?usp=drivesdk

**Video:** https://drive.google.com/drive/folders/1A7yVlkb5wBrf35HnNF57yUJO4CkxVCD_?usp=sharing

## Route optimization using Quantum annealing

_**Awarded by "Certificate of Completion"**_

**Team Member:**
- Munawar Ali

**Mentor:** Adam Glos

**Details:** https://gitlab.com/qworld/qjam/2021/-/issues/11

**Repository:** https://gitlab.com/qworld/qjam/2021/-/issues/11

**Presentation:** https://gitlab.com/qworld/qjam/2021/uploads/fb986c9e48ef3e3778f4380b0840d3a2/report.pdf

**Storyboard:** https://github.com/igelover/qolab/blob/main/docs/Storyboard.pdf


