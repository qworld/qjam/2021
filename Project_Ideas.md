![](images/qjam2021header.jpg)

Here is a list of QJamming ideas :)


| Project Idea | Keywords |
| ------------ | -------- |
| [Interactive Bloch Sphere](project_ideas/Interactive_Bloch_Sphere.md) | QEducation, QGames, QProgramming |
| [Discord Bots for Quantum](project_ideas/Discord_Bots_for_Quantum.md) | OpenQEcoSystem, QEducation, QGames, QProgramming |
| [Integrate Quantum Composer](project_ideas/Integrate_Quantum_Composer.md) | QEducation, QProgramming, QSoftware, OpenQEcoSystem |
| [Benchmarking Error Mitigation techniques](project_ideas/Benchmarking_Error_Mitigation.md) | QImplementaion, QProgramming, QSoftware, OpenQEcoSystem |
| [QTalk Ideas](project_ideas/QTalk_Ideas.md) | QEducation, QShort, OpenQEcosystem, QOutreach |
| [Animation for QFT](project_ideas/Animation_for_QFT.md) | QArt, QAnimation, QEducation, QJunior |
| [Comics for Shors Algorithm](project_ideas/Comics_for_Shors_Algorithm.md) | QArt, QComics, QEducation, QJunior |
| [Interactive Jupyter Notebooks](project_ideas/Interactive_Jupyter_Notebooks.md ) | QEducation, QGames, QFreeAccess, QProgramming, QJunior |
| [Short for Entaglement](project_ideas/Short_for_Entaglement.md) | QArt, QShort, QComics |
| [Story for Superposition](project_ideas/Story_for_Superposition.md) | QArt, QComics, QJunior |
