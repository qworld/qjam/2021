![](images/qjam2021header.jpg)

# Code of Conduct

Our event is dedicated to providing a harassment-free experience for everyone, regardless of gender, gender identity and expression, age, sexual orientation, disability, physical appearance, body size, race, ethnicity, religion (or lack thereof), or technology choices. We do not tolerate harassment of participants in any form. Sexual language and imagery are not appropriate for any event venue, including talks, Discord, Gitlab, virtual parties, Twitter and other online media. Event participants violating these rules may be sanctioned or expelled from the event.

We respect the minors (children under age 18) and we must make every effort to protect their rights. All private relationships, private communications (including social media channels), or sexual contacts with minors are prohibited.

Except the filing the application form and similar formal procedures, the contact info of any attendee or participant cannot be requested by any person from organizer side (i.e., mentor, educator, speaker, organizer, sponsor, or volunteer). On the other hand, any person from organizer side may share his or her contact info with a participant who is not a minor, upon request by the participant.

A minor can access the emails of the main organizers on the event’s website. If a minor is interested in working with a person from organizer side for scientific or pedagogical purposes, then he or she should read this document before contacting this person:  
https://qworld.net/code-of-ethics-and-conduct/#minors

If you are being harassed, notice that someone else is being harassed, or have any other concerns, please contact the organizers immediately. For any concern regarding the organizers, please contact the members of the Ethics Committee of QWorld.  
https://qworld.net/code-of-ethics-and-conduct/  
Check the above link for more details.

